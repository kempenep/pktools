version 1.0

 - First release

version 2.1
 - Introduction of configure options
	--with-fann (when FANN is installed, needed for pkclassify_nn)
	--with-las (when LIBLAS is installed, needed for pklas2img)
 - Introduction of new tools, now includes:
	pkascii2img
	pkclassify_nn
	pkcreatect
	pkcrop
	pkdiff
	pkdumpimg
	pkdumpogr
	pkegcs
	pkextract
	pkfillnodata
	pkfilter
	pkgetmask
	pkinfo
	pklas2img
	pkmosaic
	pkndvi
	pkpolygonize
	pkreclass
	pksetmask
	pksieve
	pkstat
	pkstatogr
version 2.2
	bug fixes
version 2.3
	bug fixes
version 2.4
 - configure.ac
	gdal 1.8 required for pkextract (member function Centroid in OGRMultiPolygon)
	New configure options (--enable instead of --with)
	--enable-fann (when FANN is installed, needed for pkclassify_nn)
	--enable-las (when LIBLAS is installed, needed for pklas2img)
	--enable-nlopt (when NLOPT is installed, needed for pksensormodel and pkgetchandelier)
 - OptFactory.h (added)
	factory class for nlopt::opt (selecting algorithm via string)
 - OptionPk
	changed layout of help info to Markdown format as input for Doxygen
 - Filter2d
	support filtering of matrix (doit with Vector2d arguments) (to create DTM according to P. Bunting)
 - FileReaderLas
	support class filters
 - ImgReaderGdal.h
	support of polygons in readData
 - ImgReaderGdal.cc
	in addition to internal setNoData member variable, also support GDALSetNoData
 - ImgWriterGdal.cc
	in addition to internal setNoData member variable, also support GDALSetNoData
 - ImgWriterOGR.h
	renamed ascii2shape to ascii2ogr, support csv file in ascii2ogr
 - ImgWriterOgr.cc
	renamed ascii2shape to ascii2ogr, support csv file in ascii2ogr
 - pkcrop
	changed default option for color table to empty vector
 - pkinfo
	support of nodata value when calculating histogram and image statistics
	options min and max are now set with -min (--min) and -max (--max)
	option min and max now per default empty and can be set individually for calculating histogram
	segmentation fault with option -ct if no color table was defined
	default value of x_opt empty (no longer -1 to read entire line)
 - pkfilter
	bug solved in update of progress bar
	support of standard deviation
	default empty classes
 - pkgetmask
	options min and max are now set with -min (--min) and -max (--max)
 - pkstatogr
	options min and max are now set with -min (--min) and -max (--max)
 - pkclassify_svm
	do not output input file if no input data was defined in verbose mode
	update of header information
	adding some short options
	support of polygon ogr input file
 - pkclassify_nn
	support of cross validation
	adding some short options
 - pkfs_svm (added)
	feature selection tool for svm classification
 - pkfs_nn (added)
	feature selection tool for nn classification
 - pkopt_svm (added)
	optimization tool for svm classification (optimize ccost and gamma using NLOPT)
 - pkascii2ogr
	tool to create simple vector files from coordinates in ASCII file (points or polygon)
 - pksensormodel
	tool to model pushbroom sensor (with optimization of boresight angles using NLOPT)
 - pkascii2ogr
	support csv input file
 - pklas2img
	support DTM according to Bunting (slightly adapted version of Chang2003, including a median filter after morphological operator)
