\mainpage
Introduction
============
pktools is a collection of programs written by Pieter Kempeneers in C++ to perform operations on raster images.
It heavily relies on the Geospatial Data Abstraction Library (GDAL, http://www.gdal.org) and OGR. Some of the programs are similar to the gdal tools (gdalinfo, gdal_translate, gdal_merge,...) and many of the functionalities provided in pktools already exist. The reason for implementing pktools is a combination of personal preference and additional functionality.

All utilities in pktools use command line options and have a built in help

- use the `-h` option to get help
- pktools ALWAYS use -i for input and -o for output (unlike GDAL utilities that commonly use last argument as output and second but last argument as input)

License
=======
pktools is released under the GNU General Public License version3
    
See http://www.gnu.org/licenses for more details

Download
========
You can download the latest release
- from http://download.savannah.gnu.org/releases/pktools/ (tar ball)
- or by getting a copy of the Git repository

~~~
git clone git://git.savannah.nongnu.org/pktools.git
~~~

How to refer
============
You are welcome to refer to pktools as: https://savannah.nongnu.org/projects/pktools (Pieter Kempeneers)

